#!/usr/bin/env bash

set -e -x

# do tests
######################################################################
#"${PYBIN}/nosetests" $PKG_NAME
#teststr="$( ${PYBIN}/python -c \'import $PKG_NAME; result=True if "PSFDataSet" in dir(libpsf) else False; print(result)\' )"

pip install -U tox pytest tox-venv pytest-timeout
pip install -f wheelhouse --no-cache $PKG_NAME
pytest

exit 0
