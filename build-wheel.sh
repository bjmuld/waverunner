#!/bin/bash
set -e -v

#############################################################################
#############################################################################
###
###
###         REQUIRES:  $PYBIN
###                    $PKG_NAME
###
###         THIS WILL BUILD A WHEEL FOR PYTHON SYSTEM FOUND AT $PYBIN
###
###
#############################################################################
#############################################################################

if [ -z ${PYBIN+x} ]; then
    echo 'you have not supplied $PYBIN'
    exit 1
fi

if [ -z ${PKG_NAME+x} ]; then
    echo 'you have not supplied $PKG_NAME'
    exit 1
fi

echo "***************** upgrading pip **********************"
"${PYBIN}/pip" install --upgrade pip

if [[ -r /io/dev-requirements.txt ]]; then
    echo "***************** installing dev-requirements.txt **********************"
    "${PYBIN}/pip" install -r /io/dev-requirements.txt
fi

echo "***************** building extensions **********************"
"${PYBIN}/python" /io/setup.py build_ext

echo "***************** building our wheel **********************"
"${PYBIN}/pip" wheel --no-deps -w /io/wheelhouse/ /io/

# use python 3 to audit the wheel
echo "***************** installing auditwheel **********************"
/opt/python/cp37-cp37m/bin/pip install --upgrade auditwheel

echo "***************** auditing wheels **********************"
/opt/python/cp37-cp37m/bin/python -m auditwheel repair /io/wheelhouse/*$PKG_NAME*.whl -w /io/wheelhouse/

echo "***************** removing other wheels **********************"
# if some other wheels get in the wheelhouse, remove them
( rm /io/wheelhouse/*numpy*.whl )

echo "***************** test installing our wheel **********************"
#"${PYBIN}/pip" install -f /io/wheelhouse --no-index $PKG_NAME
"${PYBIN}/pip" install -f /io/wheelhouse $PKG_NAME


# if there's a test script, run it....

if [[ -r io/test.sh ]]; then
    echo "***************** running test script **********************"
    test_result=$(bash io/test.sh && echo "passed" || echo "failed")
    if [[ "$test_result" == 'failed' ]]; then
        exit 1
    elif [[ "$test_result" == 'passed' ]]; then
        exit 0
    else
        exit 1
    fi
fi
