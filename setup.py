from setuptools import setup, find_packages
import os
import inspect
import re

name = 'waverunner'
short_description = "Package for sending (hardware) experiment requests across the network and receiving the results"
long_description = short_description

# get the full path to this file :
thispath = os.path.abspath(os.path.realpath(os.path.split(inspect.stack()[0][1])[0]))

if os.path.exists('README.md'):
    with open(os.path.join(thispath, 'README.md'), 'r') as file:
        long_description = file.read()

# read version from __init__.py :
for line in open(os.path.join(thispath, 'src/waverunner/__init__.py'), 'r').readlines():
    scraped = re.search(r'\s*__version__\s*=\s*[\"\'](\S+)[\"\']', line)
    if scraped:
        version = scraped.group(1).strip('\'\"').strip()

setup(
    name=name,
    version=version,

    entry_points={
        'console_scripts': [
            'waverunner = waverunner.Server:main',
            ],
    },

    install_requires=['netifaces',
                      'numpy',
                      'futures; python_version == "2.7"',
                      'cloudpickle',
                      'dill',
    ],

    setup_requires=['setuptools'],
    tests_require=['pytest', 'pytest-timeout'],

    packages=find_packages('src'),
    package_dir={'': 'src'},


    # metadata to display on PyPI
    author="Barry Muldrey",
    author_email="barry@muldrey.net",
    description=short_description,
    long_description=long_description,
    long_description_content_type='text/markdown',
    license="GPLv3",
    keywords="scientific experiements analysis data management",
    url="http://gitlab.com/bjmuld/waverunner/",  # project home page, if any
    project_urls={
        "Bug Tracker": "https://gitlab.com/bjmuld/waverunner/issues",
        "Documentation": "https://gitlab.com/bjmuld/waverunner",
    }
)
