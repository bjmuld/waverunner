#!/bin/bash
set -e -x

#############################################################################
#############################################################################
###
###
###         REQUIRES:
###                    $PKG_NAME
###
###         THIS WILL BUILD A WHEEL FOR PYTHON SYSTEM FOUND AT $PYBIN
###
###
#############################################################################
#############################################################################

WHEEL_DIR="$1"

ls -al "${1}"
find "${1}" -name "*${PKG_NAME}*" | xargs twine upload --verbose

#if compgen -G "$WHEEL_DIR"/"$PKG_NAME"*-manylinux1_i686.whl > /dev/null; then
#    for WHEEL in "$WHEEL_DIR"/"$PKG_NAME"*-manylinux1_i686.whl; do
#        twine upload --verbose "$WHEEL"
#    done
#fi
#
#if compgen -G "$WHEEL_DIR"/"$PKG_NAME"*-none-any.whl > /dev/null; then
#    for WHEEL in "$WHEEL_DIR"/"$PKG_NAME"*-none-any.whl; do
#        twine upload --verbose "$WHEEL"
#    done
#fi
