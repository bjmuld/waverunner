#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from __future__ import print_function
import sys
from .Server import main


if __name__ == '__main__':
    main()
    sys.exit(0)

