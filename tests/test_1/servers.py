from time import sleep
from conftest import fixtured_server_factory


sf = next(fixtured_server_factory())
servers = []


def n_servers(n):

    # timer = metrics.PrintTimesThread()
    # timer.start()

    servers = sf.get_servers(n)

    [s.start_service() for s in servers]
    servers[-1].serve_forever()
    return servers


if __name__ == '__main__':
    try:
        servers = n_servers(1)

    except Exception as e:
        raise e

    finally:

        for s in servers:

            try:
                s.exit_gracefully()

            except:
                pass



