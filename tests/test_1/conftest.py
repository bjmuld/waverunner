import os
import pytest
import threading

from waverunner import Waverunner

try:
    from . import services
except (ValueError, ImportError):
    import services

starting_port = 11234
max_n_clients = 3
max_n_servers = 10

client_ports = list(range(starting_port, starting_port+max_n_clients))
server_ports = list(range(client_ports[-1]+1, client_ports[-1] + 1 + max_n_servers))


def server_port_gen():
    n_ports = len(server_ports)
    ind = -1
    while True:
        ind += 1
        yield server_ports[ind % n_ports]


def client_port_gen():
    n_ports = len(client_ports)
    ind = -1
    while True:
        ind += 1
        yield client_ports[ind % n_ports]


s_port_gen = server_port_gen()
c_port_gen = client_port_gen()


def instantiate_a_server():

    return Waverunner(
        remote_ips=['localhost:{}'.format(client_port) for client_port in client_ports],
        interface='lo',
        port=next(s_port_gen),
        path_to_serve=os.path.split(services.__file__)[0],
        password='ratsofftoya',
        notify_interval=3,
        polling_interval=3,
        verbosity=5,
        max_n_workers=5,
    )


def instantiate_a_client():

    return Waverunner(
        remote_ips=['localhost:{}'.format(server_port) for server_port in server_ports],
        interface='lo',
        port=next(c_port_gen),
        path_to_serve=os.path.split(services.__file__)[0],
        password='ratsofftoya',
        notify_interval=0,
        polling_interval=0,
        verbosity=5,
    )


class ServerFactory(object):
    def __init__(self):
        self.living_servers = []
        self.port_ind = 0

    def get_servers(self, number):
        if number > max_n_servers:
            number = max_n_servers
            print('can only supply up to {} servers'.format(max_n_servers))

        if len(self.living_servers) < number:
            self.living_servers.extend([instantiate_a_server() for i in range(number)])
            self.port_ind = number

        return self.living_servers[:number]


class ClientFactory(object):
    def __init__(self):
        self.living_clients = []
        self.port_ind = 0

    def get_clients(self, number):
        if number > max_n_clients:
            number = max_n_clients
            print('can only supply up to {} clients'.format(max_n_clients))

        if len(self.living_clients) < number:
            self.living_clients.extend([instantiate_a_client() for i in range(number)])
            self.port_ind = number

        return self.living_clients[:number]


@pytest.fixture(scope="module")
def fixtured_server_factory():
    f = ServerFactory()
    yield f
    for s in f.living_servers:
        s.exit_gracefully()


@pytest.fixture(scope="module")
def fixtured_client_factory():
    f = ClientFactory()
    yield f
    for s in f.living_clients:
        s.exit_gracefully()


class Timer(threading.Thread):
    def __init__(self):
        super(Timer, self).__init__()
        self.stop_flag = threading.Event()
        self.stop_flag.clear()
        self.daemon = True

    def run(self):
        i = 0
        while not self.stop_flag.is_set():
            self.stop_flag.wait(1)
            i += 1
            print(i)

    def __enter__(self):
        self.run()

    def __exit__(self, exc_type, exc_val, exc_tb):
        threading.main_thread().join()

