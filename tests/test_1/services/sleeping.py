import time
import waverunner


def sleep_for_10s(phrase='ooooooohhhhhh'):
    time.sleep(10)
    return phrase


@waverunner.insecure
def sleep_for_1s(phrase='ooooooohhhhhh'):
    time.sleep(1)
    return phrase


@waverunner.secure
def sleep_for_5s(phrase='ooooooohhhhhh'):
    time.sleep(5)
    return phrase


@waverunner.insecure
def echo(phrase='i am just an echo of my former self'):
    return phrase


@waverunner.secure
def run_spice_sims(waveforms):
    pass
