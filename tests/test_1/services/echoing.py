import time
import waverunner


@waverunner.insecure
def sleep_for_10s(phrase='ooooooohhhhhh'):
    time.sleep(10)
    return phrase


@waverunner.insecure
def sleep_for_1s(phrase='ooooooohhhhhh'):
    time.sleep(1)
    return phrase


@waverunner.secure
def sleep_for_5s(phrase='ooooooohhhhhh'):
    time.sleep(5)
    return phrase


@waverunner.secure
def echo(phrase='i am just an echo of my former self'):
    return phrase


@waverunner.secure
def kwarg_test(kwarg1='i am the default first kwarg', kwarg2='i am the default kwarg2'):
    return kwarg1, kwarg2

