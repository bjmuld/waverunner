from time import sleep
from conftest import fixtured_client_factory, server_ports
from conftest import Timer


cf = next(fixtured_client_factory())
timer = Timer()

def hit_one_server_once():
    n_conc_req = 1

    client = cf.get_clients(1)[0]
    client.start_service()

    sleep(1)

    requests = []
    for i in range(n_conc_req):
        requests.append(('localhost:' + str(server_ports[0]), 'sleeping.sleep_for_5s', 'adfadfasdf'))

    timer.start()

    results = client.external_request(*requests[0])
    print('results:  {}'.format(results))
    return client

def hit_one_server_hard():
    n_conc_req = 20

    client = cf.get_clients(1)[0]
    client.start_service()

    sleep(1)

    requests = []
    for i in range(n_conc_req):
        requests.append(('localhost:' + str(server_ports[0]), 'sleeping.sleep_for_5s', 'adfadfasdf'))

    timer.start()

    results = client.threaded_external_requests(*zip(*requests))
    print('results:  {}'.format(results))
    return client


def hit_three_servers_hard():
    n_conc_req = 20

    client = cf.get_clients(1)[0]
    client.start_service()

    sleep(1)

    requests = []
    for s in range(3):
        r = []
        for i in range(n_conc_req):
            r.append(('localhost:' + str(server_ports[s]), 'sleeping.sleep_for_5s', 'adfadfasdf'))
        requests.append(r)

    results = []

    timer.start()

    results.extend(client.threaded_external_requests(*list(zip(*requests[0]))))
    results.extend(client.threaded_external_requests(*list(zip(*requests[0]))))
    results.extend(client.threaded_external_requests(*list(zip(*requests[2]))))

    print('results:  {}'.format(results))
    return client


def put_jobs_on_the_queue():
    client = cf.get_clients(1)[0]
    client.start_service()

    sleep(1)

    n_conc_req = 20

    r = []
    for i in range(n_conc_req):
        r.append(('sleeping.sleep_for_5s', 'adfadfasdf'))

    timer.start()
    jobs = []
    for rr in r:
        jobs.append(client.queue_job(*rr))

    results = []
    for j in jobs:
        results.append(client.get_result(j))

    print('result:  {}'.format(results))
    return client, timer


if __name__ == '__main__':
    try:
        #client = hit_one_server_once()
        #client = hit_three_servers_hard()
        client, timer = put_jobs_on_the_queue()

    finally:

        try:
            client.exit_gracefully()
        except:
            pass

        try:
            timer.stop()
        except:
            pass


