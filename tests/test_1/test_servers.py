from time import sleep
import pytest
import waverunner
try:
        from .conftest import instantiate_a_server, instantiate_a_client
except (ValueError, ImportError):
        from conftest import instantiate_a_server, instantiate_a_client


@pytest.mark.timeout(2)
def test_create_server_1():
        s = waverunner.Waverunner(
                port=11234,
                password='fefeefeefe',
                polling_interval=1,
                notify_interval=1,
                remote_ips=(),
        )

        assert s
        assert s.port == 11234
        assert s.password == 'fefeefeefe'
        assert s.polling_interval == 1
        assert s.notify_interval == 1
        assert s.remote_ips == None

        s.exit_gracefully()


@pytest.mark.timeout(2)
def test_create_server_2():
        s = waverunner.Waverunner(
                port=11233,
                polling_interval=1,
                notify_interval=1,
                remote_ips=(),
        )

        assert s
        assert s.port == 11233
        assert s.password == waverunner.Server.default_password
        assert s.polling_interval == 1
        assert s.notify_interval == 1
        assert s.remote_ips == None

        s.exit_gracefully()


@pytest.mark.timeout(2)
def test_create_server_3():
        s = waverunner.Server.Waverunner(
                password='fefeefeefe',
                polling_interval=1,
                remote_ips=(),
        )

        assert s
        assert s.port == waverunner.Server.default_port
        assert s.password == 'fefeefeefe'
        assert s.polling_interval == 1
        assert s.notify_interval == waverunner.Server.default_notify_interval
        assert s.remote_ips is None

        s.exit_gracefully()


@pytest.mark.timeout(2)
def test_create_server_4():
        s = waverunner.Server.Waverunner()

        assert s
        assert s.port == waverunner.Server.default_port
        assert s.password == waverunner.Server.default_password
        assert s.polling_interval == waverunner.Server.default_poll_interval
        assert s.notify_interval == waverunner.Server.default_notify_interval
        assert s.remote_ips == waverunner.Server.default_remote_ips

        s.exit_gracefully()


@pytest.mark.timeout(2)
def test_create_server_5():
        s = waverunner.Waverunner(
                port=11234,
                password='fefeefeefe',
                polling_interval=1,
                notify_interval=0,
                remote_ips=(),
        )

        assert s

        s.exit_gracefully()


@pytest.mark.timeout(2)
def test_create_server_6():
        s = waverunner.Waverunner(
                port=11234,
        )

        assert s

        s.exit_gracefully()


@pytest.mark.timeout(2)
def test_create_server_7():
        s = waverunner.Waverunner(
                password='fefeefeefe',
                polling_interval=1,
        )

        assert s

        s.exit_gracefully()


@pytest.mark.timeout(2)
def test_create_server_8():
        s = waverunner.Waverunner(
                remote_ips=[('localhost', 12345),('localhost',43223),('google.com',14321)]
        )

        assert s

        s.exit_gracefully()


@pytest.mark.timeout(2)
def test_kwargs():
        s = instantiate_a_server()
        s.start_service()

        result = waverunner.external_request(
                s.ip+':'+str(s.port),
                'echoing.kwarg_test',
                args=('kwarg1val',),
                kwargs={'kwarg2': 'kwarg2val'},
        )

        assert result == ['kwarg1val', 'kwarg2val']

        s.exit_gracefully()


@pytest.mark.timeout(10)
def test_hit_one_server_once():

        server = instantiate_a_server()
        server.start_service()

        sleep(2)

        client = instantiate_a_client()
        client.start_service()

        request = ('localhost:' + str(server.port), 'sleeping.sleep_for_5s', 'adfadfasdf')

        results = client.external_request(*request)

        print('results:  {}'.format(results))
        client.exit_gracefully()
        server.exit_gracefully()

        assert results == 'adfadfasdf'


@pytest.mark.timeout(10)
@pytest.mark.parametrize('n_concurrent_requests', [20])
def test_hit_one_server_hard(n_concurrent_requests, ):

        server = instantiate_a_server()
        server.start_service()

        sleep(1)

        client = instantiate_a_client()
        client.start_service()

        sleep(1)

        requests = []
        for i in range(n_concurrent_requests):
                requests.append(('localhost:' + str(server.port), 'sleeping.sleep_for_5s', 'adfadfasdf'))

        results = client.threaded_external_requests(*zip(*requests))

        print('results:  {}'.format(results))

        client.exit_gracefully()
        server.exit_gracefully()


@pytest.mark.timeout(40)
@pytest.mark.parametrize('n_concurrent_requests', [20])
def test_hit_three_servers_hard(n_concurrent_requests):

        servers = [instantiate_a_server() for i in range(3)]
        [s.start_service() for s in servers]

        client = instantiate_a_client()
        client.start_service()

        sleep(1)

        requests = []
        for s in servers:
                for i in range(n_concurrent_requests):
                    requests.append(('localhost:' + str(s.port), 'sleeping.sleep_for_5s', 'adfadfasdf'))

        results = client.threaded_external_requests(*list(zip(*requests)))

        client.exit_gracefully()
        [s.exit_gracefully() for s in servers]

        print('results:  {}'.format(results))


@pytest.mark.timeout(300)
@pytest.mark.parametrize('n_jobs', [20])
def test_put_jobs_on_the_queue(n_jobs):

        server = instantiate_a_server()
        server.start_service()

        client = instantiate_a_client()
        client.start_service()

        sleep(1)

        r = []
        for i in range(n_jobs):
                r.append(('sleeping.sleep_for_1s', 'adfadfasdf'))

        jobs = []
        results = []

        for rr in r:
                jobs.append(client.queue_job(*rr))

        client.block_for_queue()

        for j in jobs:
                results.append(client.get_result(j, timeout=999))

        print('result:  {}'.format(results))

        client.exit_gracefully()
        server.exit_gracefully()

        assert results == ['adfadfasdf' for i in range(len(results))]


@pytest.mark.timeout(33)
def test_single_client_multiple_servers_serial_calls():

        servers = [instantiate_a_server() for i in range(5)]
        [s.start_service() for s in servers]

        sleep(1)

        client = instantiate_a_client()
        client.start_service()

        results = []
        for s in servers:
            results.append(client.external_request('localhost:' + str(s.port), 'sleeping.sleep_for_5s', 'serverip:{}:port{}'.format(s.ip, s.port)))

        client.exit_gracefully()
        [s.exit_gracefully() for s in servers]

        print(results)

        assert results == ['serverip:{}:port{}'.format(s.ip, s.port) for s in servers]


@pytest.mark.timeout(10)
def test_single_client_multiple_servers_parallel_calls():

        servers = [instantiate_a_server() for i in range(5)]
        [s.start_service() for s in servers]

        sleep(1)

        client = instantiate_a_client()
        client.start_service()

        requests = []
        for s in servers:
            requests.append(
                ('localhost:' + str(s.port), 'sleeping.sleep_for_5s', 'serverip:{}:port{}'.format(s.ip, s.port))
            )
        requests = list(zip(*requests))

        results = client.threaded_external_requests(*requests)

        client.exit_gracefully()
        [s.exit_gracefully() for s in servers]

        print(results)
        assert results == ['serverip:{}:port{}'.format(s.ip, s.port) for s in servers]


@pytest.mark.timeout(12)
@pytest.mark.parametrize('n_requests', [10])
def test_single_client_single_server_parallel_calls(n_requests):

        server = instantiate_a_server()
        server.start_service()

        sleep(1)

        client = instantiate_a_client()
        client.start_service()

        requests = []
        for i in range(n_requests):
                requests.append(
                        ('localhost:' + str(server.port), 'sleeping.sleep_for_5s', 'iamcallnumber{}'.format(i))
                )
        requests = list(zip(*requests))

        results = client.threaded_external_requests(*requests)

        server.exit_gracefully()
        client.exit_gracefully()

        print(results)

        assert results == ['iamcallnumber{}'.format(i) for i in range(n_requests)]


if __name__ == '__main__':
        test_kwargs()
